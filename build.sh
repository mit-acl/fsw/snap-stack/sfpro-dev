#!/bin/bash

IMAGE_BOARD=mitacl/sfpro-board
IMAGE_CROSS=mitacl/sfpro-cross

# This is where ROS/CMAKE builds are pushed
ADB_DEST=/home/root

function usage() {
    echo
    echo -e "\t$0 <path> [--ros|--cmake|--bash|--load] [--ssh HX01]"
    echo
    echo -e "\t\t By default, the cross-compilation Docker image is used."
    echo -e "\t\t --ros|--cmake\tUse the sfpro-board Docker image."
    echo -e "\t\t --load\tUsed to load binaries onto device."
    echo -e "\t\t --ssh\tCan only be used with the board Docker image."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=
    LONGOPTS=ros,cmake,bash,target:,load,ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=n

    # by default we will use the cross-compilation docker image
    cross=y

    # now enjoy the options in order and nicely split until we see --
    while true; do
        case "$1" in
            --cmake)
                cmake=y
                cross=n
                shift
                ;;
            --ros)
                ros=y
                cross=n
                shift
                ;;
            --bash)
                bsh=y
                shift
                ;;
            --target)
                target="$2"
                shift 2
                ;;
            --load)
                target="load"
                load=y
                shift
                ;;
            --ssh)
                usessh=y
                ADDR="$2"
                shift 2
                ;;
            --)
                shift
                break
                ;;
            *)
                echo "Invalid options"
                usage
                exit 3
                ;;
        esac
    done

    # handle non-option arguments
    if [[ "$bsh" == "n" && $# -ne 1 ]]; then
        echo "$0: Please specify the build path"
        usage
        exit 4
    fi

    buildpath=$1
}

ros=n cmake=n bsh=n load=n
parseargs "$@"

SCRIPTSDIR=scripts

if [[ "$cross" == "y" ]]; then

    RUNSCRIPT="$SCRIPTSDIR/docker_builder_cross.sh"

    if [[ "$bsh" == "y" ]]; then
        RUNSCRIPT=
        buildpath=
        target=
    fi

    # cross builds must use adb (because of cmake rules already set up)
    if [[ "$usessh" == "y" ]]; then
        echo "$0: Cannot push cross builds via SSH (must use usb cable)"
        usage
        exit 6
    fi

    # n.b.: using the host network allows us to use adb in the container
    # without killing the adb-server on the host machine. Also bus/usb.

    docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
            -e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
            --user `id -u` \
            --group-add=plugdev \
            -v `pwd`/workspace:/home/cross/workspace:rw \
            -v `pwd`/scripts:/home/cross/scripts:rw \
            -v /dev/bus/usb:/dev/bus/usb \
            --network=host \
            ${IMAGE_CROSS} /bin/bash $RUNSCRIPT $buildpath $target

else # ROS build (or 'native' builds)

    RUNSCRIPT=
    if [[ "$ros" == "y" ]]; then
        RUNSCRIPT="$SCRIPTSDIR/docker_builder_ros.sh"
    fi
    if [[ "$cmake" == "y" ]]; then
        RUNSCRIPT="$SCRIPTSDIR/docker_builder_cmake.sh"
    fi

    if [[ "$bsh" == "y" ]]; then
        RUNSCRIPT=
        buildpath=
    fi

    docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
            -e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
            -v `pwd`/workspace:/home/root/workspace:rw \
            -v `pwd`/scripts:/home/root/scripts:rw \
            ${IMAGE_BOARD} /bin/bash $RUNSCRIPT $buildpath

    # capture return value from the runscript
    retval=$?

    # post-build installation onto snapdragon
    if [[ $retval -eq 0 && "$load" == "y" && "$bsh" != "y" ]]; then
        if [[ "$ros" == "y" ]]; then
            WS_NAME=`basename "$buildpath"`
            pushd .
            cd "$buildpath/.."

            tar zcf "${WS_NAME}.tgz" "${WS_NAME}"
            retval=$?

            if [[ $retval -eq 0 && "$usessh" == "n" ]]; then
                echo -e "\e[1mPushing \"${WS_NAME}.tgz\" to \"$ADB_DEST\" via ADB... (be patient)\e[0m"
                adb push "${WS_NAME}.tgz" "$ADB_DEST"
                adb shell rm -rf "${ADB_DEST}/${WS_NAME}"
                adb shell tar zxf "${ADB_DEST}/${WS_NAME}.tgz" -C"${ADB_DEST}"
                adb shell rm "${ADB_DEST}/${WS_NAME}.tgz"
            fi

            if [[ $retval -eq 0 && "$usessh" == "y" ]]; then
                echo -e "\e[1mPushing ${WS_NAME}.tgz to $ADDR:$ADB_DEST via SSH... (be patient)\e[0m"
                scp "${WS_NAME}.tgz" root@$ADDR:"$ADB_DEST"
                ssh root@$ADDR "rm -rf '${ADB_DEST}/${WS_NAME}' && tar zxf '${ADB_DEST}/${WS_NAME}.tgz' -C'${ADB_DEST}' && rm ${ADB_DEST}/${WS_NAME}.tgz"
            fi

            if [[ -f "${WS_NAME}.tgz" ]]; then
                # remove from host machine
                rm "${WS_NAME}.tgz"
            fi

            popd
        fi
    fi
fi
