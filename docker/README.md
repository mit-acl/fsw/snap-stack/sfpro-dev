Docker Setup
============

Intrinsyc provides a basic docker image (without the source Dockerfile) to
emulate the Snapdragon Flight Pro board. The image is an `armv7l` 32-bit
environment equipped with the `arm-oemllib32-linux-gnueabi` toolchain for
building packages to be used on the Snapdragon. In particular, this docker
image is used for building ROS packages, and then the binaries are shipped
to the Flight Pro board.

This is in contrast to the original Snapdragon Flight board, which could
compile packages on board. The Snapdragon Flight Pro however changes this
process and requires that you cross-compile packages (or use docker) on the
host machine and then push the binaries to the Snapdragon Flight Pro.

## Board Docker Dependencies (Cross-Compilation for ROS)

The following are added to the base docker image from Intrinsyc.

- Eigen
- mv 1.2.7 *(conditionally)*
