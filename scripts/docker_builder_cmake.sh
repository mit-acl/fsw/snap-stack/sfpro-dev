#!/bin/bash

#
# This sfpro docker build expects that BUILDPATH puts you next to a
# CMakeLists.txt from which you will create a build dir and run the
# usual 'cmake ..' command.
#

BUILDPATH=$1

cd "$BUILDPATH"

mkdir build
cd build && cmake ..
make
