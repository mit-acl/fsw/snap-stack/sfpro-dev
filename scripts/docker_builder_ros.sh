#!/bin/bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
WHITE='\033[1;37m'
ENDCOLOR='\033[0m'

#
# This sfpro docker build expects that BUILDPATH puts you in a
# catkin_ws, right next to a 'src' directory.
#

BUILDPATH=$1
ROSWSDIR=`basename "$BUILDPATH"`

# Take care of dependencies and preconditions
FAILED=0
function handle_precond {
    bash $1 workspace kyro
    rv=$?
    printf "${WHITE}%-60s${ENDCOLOR}" $1
    if [[ $rv -eq 0 ]]; then
        printf "${GREEN}success${ENDCOLOR}\n"
    else
        printf "${RED}failed${ENDCOLOR}\n"
        FAILED=1
    fi
}
echo
echo -e "${YELLOW}Resolving Preconditions${ENDCOLOR}"
find "$BUILDPATH" -name '.precond.sh' > files
while IFS= read -r file; do
    handle_precond $file
done < files
echo
if [[ $FAILED -ne 0 ]]; then
    echo -e "${RED}Failed to meet all preconditions."
    echo
    exit 1
fi

# create a symlink so that our paths are exactly the same as on the snapdragon
ln -s "$BUILDPATH"

cd "$ROSWSDIR"

source /opt/ros/indigo/setup.bash
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release -DCMAKE_FIND_ROOT_PATH=/./ -DQC_SOC_TARGET=APQ8096
catkin build
retval=$?

# let the calling script (outside of docker) know how building went
exit $retval
