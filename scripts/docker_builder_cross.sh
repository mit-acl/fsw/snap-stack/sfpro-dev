#!/bin/bash

#
# This sfpro docker build expects that BUILDPATH puts you next to a Makefile
# which will be used to kick off the cross-compilation task.
#

BUILDPATH=$1
TARGET=$2

cd "$BUILDPATH"

make QC_SOC_TARGET=APQ8096 $TARGET
