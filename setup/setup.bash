#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source $MYPATH/utils

echo
echo_yel "\tRemember to first flash the OS using \`sudo ./jflash.sh\`."
echo_yel "\t    Use version 1.0 (18 Jan 2019) from Intrinsyc."
echo

read -p $'\e[1;37mVehicle type (e.g., `HX`): \e[0m' VEHICLE_TYPE
read -p $'\e[1;37mVehicle num (e.g., `01`): \e[0m' VEHICLE_NUM
VEHICLE="$VEHICLE_TYPE$VEHICLE_NUM"
GCS_IP="192.168.0.19"

# First, verify that there is a Snapdragon plugged in; exit if not
findQualcommDevice

# Make sure that this is a Snapdragon Flight Pro (Excelsior 820 / APQ8096)
apqid=$( adb shell getprop ro.product.name |  tr -d '[:space:]' )
if [[ "$apqid" != "apq8096-drone" ]]; then
  fail "Please run while connected to a Snapdragon Flight Pro 820 board"
  exit
fi

echo

#
# Setup bashrc / home environment
#

if [[ $(adb shell cat $HOME/.bashrc) != *"export HOME"* ]]; then
    message "Pushing .bashrc"

    adb push $MYPATH/bashrc /home/root/.bashrc
    adb shell "sed -i s/_ROSMASTER_/$GCS_IP/g /home/root/.bashrc"
    adb shell "sed -i s/_VEHTYPE_/$VEHICLE_TYPE/g /home/root/.bashrc"
    adb shell "sed -i s/_VEHNUM_/$VEHICLE_NUM/g /home/root/.bashrc"

    # ssh sources this, which sources the bashrc
    adb push $MYPATH/bash_profile /home/root/.bash_profile

    # setting /bin/bash as default shell
    adb shell chsh root -s /bin/bash

    echo
fi

#
# Setup networking
#
message "Setting up networking for $YELLOW$VEHICLE"

wlan_mode=$(adb shell cat /data/misc/wifi/wlan_mode | tr -d '[:space:]')
if [[ "$wlan_mode" != "station" ]]; then
    adb shell "echo station > /data/misc/wifi/wlan_mode"
    adb reboot

    echo_info "Rebooting device into STATION mode..."
    sleep 30
    findQualcommDevice
fi

# Hostname
adb shell "echo $VEHICLE > /etc/hostname"
adb shell "echo 127.0.0.1 localhost.localdomain localhost $VEHICLE > /etc/hosts"

# SSID / passphrase to robot wifi
read -p $'\e[1;37mHighbay SSID: \e[0m' netssid
read -p $'\e[1;37mSSID Passphrase: \e[0m' netpsk

# WPA supplicant for wifi comms
adb push $MYPATH/wpa_supplicant /data/misc/wifi/wpa_supplicant.conf
adb shell "sed -i s/_NETSSID_/$netssid/g /data/misc/wifi/wpa_supplicant.conf"
adb shell "sed -i s/_NETPSK_/$netpsk/g /data/misc/wifi/wpa_supplicant.conf"

echo_info "Waiting for connection to mitnet"
adb shell wpa_cli reconfigure
adb shell wpa_cli sel 0
adb shell wpa_cli stat
adb shell sleep 15
adb shell wpa_cli stat

# Turn off wifi power saving (which can introduce latency)
adb shell iw dev wlan0 set power_save off

#
# Install router script for advanced networking
#

ROUTER_FILE=/etc/init.d/router
ROUTER_EXISTS=$(adb shell "if [ -d '$ROUTER_FILE' ]; then echo 'exists'; else echo ''; fi;")
if [[ "$ROUTER_EXISTS" != "exists" ]]; then
    adb push $MYPATH/router $ROUTER_FILE

    # change default local subnet to be same as vehicle number
    adb shell "sed -i s/_VEHNUM_/$VEHICLE_NUM/g $ROUTER_FILE"
    adb shell "chmod +x $ROUTER_FILE"

    # install the SysV service to run on boot
    adb shell "update-rc.d router defaults"
fi


#
# Set up services
# 

if [[ -d "$MYPATH/services" ]]; then
  message "Pushing services"
  
  adb push $MYPATH/services /home/root/services
  
  message "Enabling services"
  adb shell 'for filename in /home/root/services/*.service; do systemctl enable $filename; done'
fi

#
# Miscellaneous Configuration
#

# set blank root password (on first ssh this is required again for some reason)
adb shell passwd -d root

# prevent git from using colors, which is unsupported
adb shell HOME=/home/root git config --global color.diff false

#
# If present in this dir, install license and mv lib
#

SFLIC=snapdragon-flight-license.bin
SFMV=mv_1.2.10_8x96.ipk

if [[ -f $MYPATH/$SFLIC ]]; then
    message "Pushing $SFLIC to $YELLOW$VEHICLE"

    adb shell mkdir -p /opt/qcom-licenses
    adb push $MYPATH/$SFLIC /opt/qcom-licenses/
    adb shell sync

    echo
fi

if [[ -f $MYPATH/$SFMV ]]; then
    message "Pushing $SFMV to $YELLOW$VEHICLE"

    adb shell mkdir -p /data/bin
    adb push $MYPATH/$SFMV /data/bin
    adb shell opkg install /data/bin/$SFMV > /dev/null

    echo
fi

#
# Install Avahi service for '.local' discovery
#

AVAHI_DIR=install-avahi
AVAHI_TGZ=avahi-install_0.0.2.tar.gz
if [[ ! -d "$MYPATH/$AVAHI_DIR" ]]; then

    # use ModalAI VOXL repo
    git clone https://gitlab.com/voxl-public/install-avahi.git $MYPATH/$AVAHI_DIR > /dev/null

    # build tar gz
    pushd .
    cd $MYPATH/$AVAHI_DIR && ./create-installer.sh > /dev/null
    popd
fi

message "Pushing $AVAHI_TGZ to $YELLOW$VEHICLE"
adb shell mkdir -p /data/bin
adb push $MYPATH/$AVAHI_DIR/$AVAHI_TGZ /data/bin
adb shell "tar zxf /data/bin/$AVAHI_TGZ --directory /data/bin/"
adb shell "cd /data/bin/avahi-install && ./install.sh" > /dev/null

#
# Push git commit hash to vehicle so we know what version was used to setup
#

GIT_VERSION_STRING=$(cd $MYPATH ; git describe --tags --abbrev=8 --always --dirty --long)
adb shell "echo '$(date)' > /sfpro-dev-setup"
adb shell "echo '$GIT_VERSION_STRING' >> /sfpro-dev-setup"

# -----------------------------------------------------------------------------
# Success!
success "Setup completed -- rebooting"
adb reboot

