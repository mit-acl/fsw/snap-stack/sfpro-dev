Snapdragon Flight Pro - Development Environment
===============================================

**ACL Users**: You can download the files below from [this](https://www.dropbox.com/sh/1553npmpuw3ha2w/AACk809EGnO22jmUHT_Oh3tYa?dl=0) password-protected link (hint: sikorsky).

The purpose of this repo is to (1) flash and (2) setup and (3) push code to the Flight Pro board. Make sure to look at the [wiki](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev/-/wikis/home) for information on why this is important and on working with the Snapdragon Flight Pro. To perform the flashing and setup steps outlined below, the Flight Pro **must be connected via micro USB** (see the [wiki](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev/-/wikis/02.-adb-shell)).

## Initial Flashing and Setup

1. Download `QualcommFlightPro_APQ8096-LE-1-0-1-r00032.2_Jflash.zip` from Intrinsyc and unzip.
2. Power on and connect Snapdragon Flight 820 board and run `sudo ./jflash.sh` (see [Troubleshooting](#troubleshooting)).
3. You may need to restart the adb server with root permissions: `sudo adb kill-server && sudo adb devices` (see [Wiki](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev/-/wikis/02.-adb-shell)).
4. (Optional) Place `snapdragon-flight-license.bin` and `mv_1.2.7_8x96.ipk` into the `setup` directory. This is for the machine vision library and will be automatically installed if present.
4. ACL setup: `cd setup` and `./setup.bash`

## Environment Setup

### Docker - Board Emulation for ROS development

1. Download `QualcommFlightPro_APQ8096-LE-1-0-1-r00032.2_Docker.zip` from Intrinsyc and unzip.
2. Load the image into docker: `docker load < excelsior-arm-sdk-sfpro_docker.tar`.
3. Based on the [Intrinsyc wiki](https://tech.intrinsyc.com/projects/qualcomm-flight-pro-development-kit/wiki/docker), if you see an **"exec format error"**, `sudo apt install qemu-user-static`.
4. In the root directory of this project (e.g., `~/sfpro-dev/`), run `make board`.
5. The `mitacl/sfpro-board` image will be created.

See [docker/README.md](docker/README.md) for more information.

### Docker - Hexagon SDK and ARM Cross Compiler for SLPI development and Kyro development

*Note:* The board emulation docker can also be used to compile Kyro (i.e., CPU) apps. However, we recommend using this image for both Kyro and SLPI cross-compilation and using the board emulation image specifically for ROS compilation or any other code that needs to be linked against libraries on the target.

1. Download [qualcomm_hexagon_sdk_3_1_eval.bin](https://developer.qualcomm.com/qfile/33835/qualcomm_hexagon_sdk_3_1_eval.bin) from Qualcomm and place in the `workspace` subdirectory of this project.
2. Download [QualcommFlightPro_APQ8096-LE-1-0-1-r00032.2_CrossCompiler.zip](https://tech.intrinsyc.com/attachments/download/5778/QualcommFlightPro_APQ8096-LE-1-0-1-r00032.2_CrossCompiler.zip) from Intrinsyc and place in the `workspace` subdirectory of this project.
3. In the root directory of this project (e.g., `~/sfpro-dev/`), run `make cross`.
4. The `mitacl/sfpro-cross` image will be created.

## Usage

One the Docker images have been created, the `build.sh` script is used to cross-compile packages in the `workspace` subdirectory.

### Examples

```bash
# Build dspal_tester and version_test (without pushing)
~/sfpro-dev $ ./build.sh workspace/dspal/test
```

```bash
# Build and push esc_interface (via adb/usb)
~/sfpro-dev $ ./build.sh workspace/esc_interface --load
```

```bash
# Drop into the Docker cross-compilation environment
~/sfpro-dev $ ./build.sh --bash
```

```bash
# catkin_make ROS workspace and then push via usb/adb
~/sfpro-dev $ ./build.sh workspace/acl_ws --ros --load
```

```bash
# catkin_make ROS workspace and then push via ssh to host hx01
~/sfpro-dev $ ./build.sh workspace/acl_ws --ros --load --ssh hx01
```

### Preconditions

Because ROS applications are built in a separate Docker container than DSPAL libs, some dependency management must be performed. This happens in the form of `.precond.sh` files located in the build path (i.e., the ROS workspace to be built). These `.precond.sh` scripts are automatically found and run by `docker_builder_ros.sh` and can be used to "install" (i.e,. copy) the necessary libraries, header files, etc to the correct location in the `sfpro-board` Docker container for building.

For example, the ACL [snap](https://github.com/mit-acl/snap) stack requires the `libesc_interface.so` and header files to be in the `PATH`. Therefore, the snap stack must have a `.precond.sh` located in the `snap` package that correctly copies these files over. This requires that (1) the `esc_interface` project is also in the `sfpro-dev/workspace` and that it has already been built.

### Systemd Services

Systemd service files are an excellent way to perform tasks on boot or kickoff system-level processes. For example, since the `imu_app` is a daemon that allows the CPU to access the IMU via the DSP, an `imu.service` tells the system to start the `imu_app` daemon on boot. The following commands are useful to know when working with systemd services:

```bash
$ systemctl enable imu  # service will start on boot. If not in system location, provide full path to service file.
$ systemctl disable imu # service will not start on boot.
$ systemctl start imu   # starts the imu service (not necessary if enabled and have just booted)
$ systemctl stop imu    # stop the imu service
$ systemctl status imu  # check if serivce is running, shows any output from process
```

When using `setup.bash`, and service files in `setup/services` will automatically be pushed onto the sfpro and enabled.

### Troubleshooting

1. If you cannot use `jflash.sh` to successfully flash the Snapdragon, you may need to [change](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev/-/wikis/01.-jflash) `adb` to `$adbcmd` in `jflash.sh:167`. The resulting line would be
    ```bash
    modelIdApq=$( $adbcmd shell getprop ro.product.name |  tr -d '[:space:]' )
    ```
2. Make sure to use `sudo adb devices` the first time after flashing to start the adb server with proper permissions. See the [wiki](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev/-/wikis/02.-adb-shell) for more.
3. If during `setup.bash` you get any **error: device not found** messages, kill `setup.bash` and run it again.

### FAQ

1. How do I use `catkin clean` or change the specific catkin commands?
    - You can temporarily edit the `scripts/docker_builder_ros.sh` script and add e.g., `catkin clean snapstack_msgs` just prior to the `catkin config` line. But don't commit these types of changes since they are specific to you!
