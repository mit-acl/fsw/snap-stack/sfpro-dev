IMAGE_BOARD:="mitacl/sfpro-board"
IMAGE_CROSS:="mitacl/sfpro-cross"

.PHONY: all
all: board cross

.PHONY: board
board:
	@docker build -t $(IMAGE_BOARD) -f docker/Dockerfile.board .

.PHONY: board-bash
board-bash:
	@docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
		-e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
		-w /home/root \
		$(IMAGE_BOARD) /bin/bash

.PHONY: cross
cross:
	@docker build -t $(IMAGE_CROSS) -f docker/Dockerfile.cross .

.PHONY: cross-bash
cross-bash:
	@docker run --rm -it --privileged -e LOCAL_USER_ID=`id -u` \
		-e LOCAL_USER_NAME=`echo ${USER}` -e LOCAL_GID=`id -g` \
		--user `id -u` \
		--group-add=plugdev \
		-v /dev/bus/usb:/dev/bus/usb \
		--network=host \
		$(IMAGE_CROSS) /bin/bash

.PHONY: mini-dm
mini-dm:
	@docker run --rm -it --privileged -u 0 \
		$(IMAGE_CROSS) /bin/bash -c "\$${HEXAGON_SDK_ROOT}/tools/debug/mini-dm/Linux_Debug/mini-dm"
